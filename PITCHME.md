# AWS Elasticache

##### The 10 page chapter that everyone thought should have been 3 pages...

---

## Y U NO Cache?

>Lies, damned lies, and statistics.

* 2007 testing of Amazon.com showed every 100ms in load times decreased sales by 1%
* Nielsen Norman Group [guidelines](https://www.nngroup.com/articles/response-times-3-important-limits/):
	* 0.1 second: user feels the system is instantaneous
	* \>1.0 second: user's flow of thought interrupted
	* \>10 seconds: the user is not focused on dialogue

---

## There are 10 Kinds of Caching

* Redis
* -Memcached-

ElastiCache is transparent to your application, just update your app config endpoint

---

## ElastiCache Nodes

* ElastiCache deployments have 1+ nodes in a cluster
* Nodes are a subset of EC2 instance types :
	* 555MB memory for a t2.micro
	* => 237GB memory for an r3.8xlarge

---

### ElastiCache Clusters

* Consists of 1 or more nodes
* Up to 20 nodes in a cluster for Memcached
* Only 1 node in a cluster for Redis

---

### Memcached Auto Discovery

For Memcached clusters partitioned w/ multiple nodes:
* ElastiCache supports Auto Discovery w/ client lib
* App no longer needs to know about infrastructure topology

---

### Scaling

* Horizontal scaling is supported for both Memcached & Redis
	* Memcached partitions over multiple nodes
	* Redis supports up to 5 read replicas
* Vertical scaling requires a node change type
	* With Memcached you'll lose your entire cache
	* Redis can restore the cache from a backup

---

### Backup & Recovery

* Memcached cannot be backed up or restored
* Redis can persist in-memory data natively
	* This backup file is stored in S3
* You can create snapshots (manually or scheduled)
	* But why?

---

### Security

* ElastiCache access is controlled through network access by:
	* Security groups
	* Network ACL
	* API actions via IAM

---

## Review Questions

a.k.a. I didn't finish my presentation so I did these first...

+++

#### 1. Which of the following objects are good candidates to store in a cache? (Choose 3)

* <span class="fragment highlight-blue visible">A) Session state</span>
* <span class="fragment highlight-blue visible">B) Shopping cart</span>
* <span class="fragment highlight-blue visible">C) Product catalog</span>
* D) Bank account balance

+++

##### 2. Which of the following cache engines are supported by Amazon ElastiCache? (Choose 2)

* A) MySQL
* <span class="fragment highlight-blue visible">B) Memcached</span>
* <span class="fragment highlight-blue visible">C) Redis</span>
* D) Couchbase

+++

###### 3. How many nodes can you add to an Amazon ElastiCache cluster running Memcached?

* A) 1
* B) 5
* <span class="fragment highlight-blue visible">C) 20</span>
* D) 100

+++

###### 4. How many nodes can you add to an Amazon ElastiCache cluster running Redis?

* <span class="fragment highlight-blue visible">A) 1</span>
* B) 5
* C) 20
* D) 100

+++

###### 5.What steps are required to migrate an app using Memcached to store frequently used DB queries to use Amazon ElastiCache with minimal changes? (Choose 2)

* A) Recompile the application to use the Amazon ElastiCache libraries
* <span class="fragment highlight-blue visible">B) Update the configuration file with the endpoint for the Amazon ElastiCache cluster</span>
* <span class="fragment highlight-blue visible">C) Configure a security group to allow access from the applicaton servers</span>
* D) Connect to the Amazon ElastiCache nodes using SSH and install the latest version of Memcached

+++

##### 6. How can you back up data stored in Amazon ElastiCache running Redis? (Choose 2)

* A) Create an image of the EC2 instance
* <span class="fragment highlight-blue visible">B) Configure automatic snapshots to back up the cache environment every night</span>
* <span class="fragment highlight-blue visible">C) Create a snapshot manually</span>
* D) Redis clusters cannot be backed up

+++

##### 7. How can you secure an Amazon ElastiCache cluster? (Choose 3)

* A) Change the Memcached root password
* <span class="fragment highlight-blue visible">B) Restrict API actions using AWS IAM policies</span>
* <span class="fragment highlight-blue visible">C) Restrict network access using security groups</spna>
* <span class="fragment highlight-blue visible">D) Restrict network acess using a network ACL</span>

+++

##### 8. Which AWS services are best suited for supporting a leaderboard in a mobile game to track the top scores across millions of users?

* A) Amazon Redshift
* B) Amazon ElastiCache using Memcached
* <span class="fragment highlight-blue visible">C) Amazon ElastiCache using Redis</span>
* D) Amazon S3

+++

##### 9. You plan to expand both your web fleet and your cache fleet (ElastiCache using Memcached) over the next year due to increased user traffic. How do you minimize the amount of changes required when this scaling event occurs?

* <span class="fragment highlight-blue visible">A) Configure AutoDiscovery on the client side</span>
* B) Configure AutoDiscovery on the server side
* C) Update the configuration file each time a new cluster
* D) Use an Elastic Load Balancer to proxy the requests
